import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMaterialModule } from './material-design.module';
import { SearchFilterPipe } from './pipes/search-filter.pipes';

@NgModule({
  imports: [
    CommonModule,
    CustomMaterialModule,
  ],
  exports: [
    CommonModule,
    CustomMaterialModule,
    SearchFilterPipe
  ],
  declarations: [
      SearchFilterPipe
  ],
  providers: [ ],
})
export class SharedModule { }
