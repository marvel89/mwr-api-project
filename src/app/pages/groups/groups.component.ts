import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { MatDialog, MatSnackBar} from '@angular/material';
import { Subscription } from 'rxjs';
import { ICategories } from '@shared/interfaces/category.interface';
import { STORAGE_VARIABLES, StorageService } from '@core/services/storage.service';
import { API_END_POINTS, CustomEncoder, DataService } from '@core/services/data.service';
import { HelperService } from '@core/services/helper.service';
import { ViewGroupDetailsModalComponent } from '@uikit/modals/view-group-details-modal/view-group-details-modal.component';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit, OnDestroy {
  is_loading: boolean = true;
  search_group: string;
  other_selected_categories: ICategories[];
  preferred_category: ICategories;
  empty_category: boolean = false;
  dialog_subscription: Subscription;

  constructor(private data_service: DataService,
              private storage_service: StorageService,
              private helper_service: HelperService,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) {
      this.preferred_category = this.storage_service.getLocalStorageObject(STORAGE_VARIABLES.PREFERRED_CATEGORY);
      this.other_selected_categories = this.storage_service.getSessionStorageObject(STORAGE_VARIABLES.OTHER_CATEGORIES);
  }

  ngOnInit() {
      // only fetch groups if user selected at least one category
      if (this.preferred_category.id || this.other_selected_categories.length > 0) {
          this.fetchCategories();
      } else {
          this.empty_category = true;
          this.is_loading = false;
      }
  }

  ngOnDestroy() {
    if (this.dialog_subscription) { this.dialog_subscription.unsubscribe(); }
  }

  fetchCategories(): void  {
      let params = new HttpParams({ encoder: new CustomEncoder() });
      params = params.append('country', 'ZA');
      params = params.append('location', 'gauteng');
      params = params.append('category', this.helper_service.formatCategoriesForAPIParam());
      this.data_service.dataGet(API_END_POINTS.GET_GROUPS, params)
      .subscribe((response: any) => {
              this.is_loading = false;
              this.processRequest(response);
      },
      (err: any) => {
          console.log(err);
          this.snackBar.open('An error happened while connecting to the api', 'Ok', { duration: 5000 });
      });
  }

  processRequest(response) {
      if (response && response.length > 0) {
          if (this.preferred_category) {
              this.preferred_category.groups = [];
              response.forEach( (group) => {
                  if (this.preferred_category && (group.category.id === this.preferred_category.id)) {
                      this.preferred_category.groups.push(group);
                  }
              });
          }
          if (this.other_selected_categories.length > 0) {
              if (this.other_selected_categories.length > 0) {
                  this.other_selected_categories.forEach( (other_category) => {
                      other_category.groups = [];
                      response.forEach( (group) => {
                          if (group.category.id === other_category.id) {
                              other_category.groups.push(group);
                          }
                      });
                  });
              }
          }
      }
  }

  groupClicked(group) {
      const dialogRef = this.dialog.open(ViewGroupDetailsModalComponent, {
          disableClose: false,
          data: group,
          panelClass: 'view-group-modal',
          height: '410px'
      });
      this.dialog_subscription = dialogRef.afterClosed().subscribe(result => { });
  }
}
