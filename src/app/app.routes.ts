import { Routes } from '@angular/router';
import { SettingsPageComponent } from '@pages/settings-page/settings-page';
import { OtherCategoriesComponent } from '@pages/other-categories/other-categories.component';
import { GroupsComponent } from '@pages/groups/groups.component';

const routes: Routes = [
  { path: 'settings', component: SettingsPageComponent },
  { path: 'other-categories', component: OtherCategoriesComponent },
  { path: 'groups', component: GroupsComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'settings' },
];

export const appRouting: any = {
  routes: routes,
  components: [
      SettingsPageComponent,
      OtherCategoriesComponent,
      GroupsComponent,
  ],
  entryComponents: [ ]
};
