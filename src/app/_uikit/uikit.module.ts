import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { MODALS } from './modals';

@NgModule({
    declarations: [
        MODALS
    ],
    imports: [
        CommonModule,
        SharedModule,
        CoreModule
    ],
    exports: [
        MODALS
    ],
    entryComponents: [
        MODALS
    ]
})
export class UikitModule {
}
