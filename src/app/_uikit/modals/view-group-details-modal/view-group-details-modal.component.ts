import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GroupsComponent } from '@pages/groups/groups.component';

@Component({
  selector: 'app-view-group-details-modal',
  templateUrl: './view-group-details-modal.component.html',
  styleUrls: ['./view-group-details-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ViewGroupDetailsModalComponent implements OnInit {

    constructor (
    public dialogRef: MatDialogRef<GroupsComponent>,
    @Inject(MAT_DIALOG_DATA) public group_data: any) {}

    ngOnInit() { }

    closeModal(): void {
        this.dialogRef.close({ action: 'closed', data: null });
    }

}
