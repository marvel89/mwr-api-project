import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewGroupDetailsModalComponent } from './view-group-details-modal.component';

describe('ViewGroupDetailsModalComponent', () => {
  let component: ViewGroupDetailsModalComponent;
  let fixture: ComponentFixture<ViewGroupDetailsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGroupDetailsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewGroupDetailsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
