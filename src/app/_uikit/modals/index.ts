import { ChangePreferredCategoryModalComponent} from './change-preferred-category-modal/change-preferred-category-modal.component';
import { ViewGroupDetailsModalComponent} from './view-group-details-modal/view-group-details-modal.component';

export const MODALS: any[] = [
    ChangePreferredCategoryModalComponent,
    ViewGroupDetailsModalComponent
];
