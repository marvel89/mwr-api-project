import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePreferredCategoryModalComponent } from './change-preferred-category-modal.component';

describe('ChangePreferredCategoryModalComponent', () => {
  let component: ChangePreferredCategoryModalComponent;
  let fixture: ComponentFixture<ChangePreferredCategoryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePreferredCategoryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePreferredCategoryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
