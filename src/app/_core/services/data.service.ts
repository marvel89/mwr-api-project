import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParameterCodec, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { API_END_POINT } from '@shared/constants/api.constants';
import { environment } from '@env/environment';
import { catchError } from 'rxjs/operators';

@Injectable()
export class DataService {
    baseUrl: string = 'https://cors-anywhere.herokuapp.com/https://api.meetup.com/';
    // Using Heroku app proxy to bypass cors rules
    // Ref: (https://stackoverflow.com/questions/44448177/no-access-control-allow-origin-angular-4-app)

    constructor(private http: HttpClient) { }

    dataGet(route?: string, params?: HttpParams): Observable<any> {
        if (!params) params = new HttpParams({ encoder: new CustomEncoder() });
        params = params.append('key', environment.meetUpApiKey);
        return this.http.get(this.baseUrl + route, { params })
        .pipe(catchError(this.handleError));
    }

    dataPost(route?: string, body?: HttpParams): Observable<any> {
        if (!body) body = new HttpParams({ encoder: new CustomEncoder() });
        body = body.append('key', environment.meetUpApiKey);
        return this.http.post(this.baseUrl + route, body, { headers: this.setPostHeader() })
        .pipe(catchError(this.handleError));
    }

    private setPostHeader(): HttpHeaders {
        return new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        });
    }

    private handleError(error: any) {
        console.error('server error:', error);
        if (error instanceof Response) {
            let errMessage: any;
            try {
                errMessage = error.json().catch((err: any) => err);
            } catch (err) {
                errMessage = error.statusText;
            }
            return throwError(errMessage);
        }
        return throwError(error || 'server error');
    }
}


export const API_END_POINTS = API_END_POINT;

// Angular convert any + to space, this encode would convert to ASCII Space
// Reference https://github.com/angular/angular/issues/18261 :)
export class CustomEncoder implements HttpParameterCodec {
    encodeKey(key: string): string {
        return encodeURIComponent(key);
    }

    encodeValue(value: string): string {
        return encodeURIComponent(value);
    }

    decodeKey(key: string): string {
        return decodeURIComponent(key);
    }

    decodeValue(value: string): string {
        return decodeURIComponent(value);
    }
}
