import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClientJsonpModule  } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { DataHttpInterceptor } from './interceptor/data.interceptor';
import { DataService } from './services/data.service';
import { StorageService } from './services/storage.service';
import { HelperService } from './services/helper.service';

@NgModule({
  imports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [],
  providers: [
    DataService,
    StorageService,
    HelperService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DataHttpInterceptor,
      multi: true
    },
  ],
})
export class CoreModule { }
